<?php

namespace app\modules\api_v1\models\forms;


use app\modules\api_v1\models\Files;

class FileSearchForm extends \yii\base\Model
{
    public $id;
    public $_file = false;

    public function rules()
    {
        return [
            [['id'], 'required'],
            ['id', 'integer'],
            ['id', 'validateId'],
        ];
    }

    public function validateId()
    {
        if (!$this->hasErrors()) {
            if (!$this->getFile()) {
                $this->addError('id', 'Файл не найден');
            }
        }

    }

    public function getFile()
    {
        if ($this->_file === false)
            $this->_file = Files::find()->where(['id' => $this->id])->one();

        return $this->_file;
    }

}