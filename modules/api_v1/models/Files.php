<?php

namespace app\modules\api_v1\models;

class Files extends \app\models\Files
{
    public function fields()
    {
        return [
            'id',
            'Name' => 'name',
            'Created' => 'created_at',
        ];
    }

}