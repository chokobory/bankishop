<?php

namespace app\modules\api_v1\controllers;

use app\modules\api_v1\models\Files;
use app\modules\api_v1\models\forms\FileSearchForm;
use yii\helpers\Json;
use yii\web\Controller;

class FilesController extends Controller
{
    public function actionIndex()
    {
        return Json::encode(Files::find()->all());
    }

    public function actionGetFile($id)
    {
        $model = new FileSearchForm(['id' => $id]);

        if (!$model->validate())
            return Json::encode(["Некорректный ID" => implode(', ', $model->getFirstErrors())]);

        return Json::encode($model->getFile());
    }
}
