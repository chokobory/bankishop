<?php

namespace app\controllers;

use app\models\Files;
use app\models\forms\FilesSearch;
use app\models\forms\UploadForm;
use Exception;
use Throwable;
use Yii;
use yii\base\ExitException;
use yii\base\InvalidRouteException;
use yii\db\StaleObjectException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use ZipArchive;

/**
 * FileController implements the CRUD actions for Files model.
 */
class FileController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }


    public function actionIndex()
    {
        $searchModel = new FilesSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }


    public function actionCreate()
    {
        $model = new UploadForm();

        if ($this->request->isPost) {
            if ($model->load($this->request->post())) {
                $model->files = UploadedFile::getInstances($model, 'files');
                $model->saveImage();
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }


    /**
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }


    /**
     * @throws Throwable
     * @throws StaleObjectException
     * @throws NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * @throws Exception
     */
    public function actionDownloadFile()
    {
        $file = Yii::$app->request->post('file');

        $filename =  Yii::getAlias('@app') . "/web/$file";

        if (!file_exists(trim($filename)))
            self::fileNotFound();

        $zipName = Yii::getAlias('@app') . '/' . date('Yd_m_Y_H_i_s') . '.zip';
        $zip = new ZipArchive();

        if (!$zip->open($zipName, ZipArchive::CREATE))
            throw new Exception('Failed to create ZIP archive');

        $zip->addFile($filename, basename($filename));
        $zip->close();
        Yii::$app->response->sendFile($zipName);
        unlink($zipName);
    }


    /**
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = Files::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * @throws ExitException
     * @throws InvalidRouteException
     */
    private static function fileNotFound(): void
    {
        Yii::$app->session->setFlash('error', 'Изображение не найдено');
        $url = Yii::$app->request->referrer;
        Yii::$app->getResponse()->redirect($url);
        Yii::$app->end();
    }

}
