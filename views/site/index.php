<?php

/** @var yii\web\View $this */

$this->title = 'Главная';
?>
<div class="site-index">
    <div class="jumbotron text-center bg-transparent mt-5 mb-5">
        <h1 class="display-4">Хостинг изображений</h1>

        <p class="lead">Здесь можно увидеть список загруженных изображений, скачать и загрузить</p>

        <p><a class="btn btn-lg btn-success" href="/file/">Перейти к списку</a></p>
    </div>
</div>
