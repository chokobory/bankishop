<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var app\models\forms\FilesSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Список изображений';
$this->params['breadcrumbs'][] = $this->title;

$css = <<<CSS
.pagination {
    margin-top: 20px;
    justify-content: center;
}
CSS;

$this->registerCss($css);

?>
<div class="files-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить изображение', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            [
                'attribute' => 'path',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a(
                        Html::img($model->path, ['width' => '100px']),
                        $model->path,
                        ['target' => '_blank']
                    );
                },
            ],
            [
                'attribute' => 'created_at',
                'value' => function ($data) {
                    $date = new DateTime($data->created_at);
                    return $date->format('d.m.Y H:i:s');
                },
            ],
            [
                'label' => 'Скачать',
                'format' => 'raw',
                'contentOptions' => ['style' => 'width: 120px; justify-content: center; align-items: center;'],
                'value' => function ($data) {
                    return Html::beginForm(Url::to(['file/download-file']), 'post') .
                        Html::hiddenInput('file', $data->path) .
                        Html::submitButton('Скачать изображение', [
                            'class' => 'btn btn-primary',
                            'onclick' => 'event.preventDefault(); this.form.submit();',
                        ]) .
                        Html::endForm();
                },
            ],

        ],
        'pager' => [
            'class' => \yii\widgets\LinkPager::class,
            'options' => [
                'class' => 'pagination',
                'activePageCssClass' => 'page-item active',
                'disabledPageCssClass' => 'disabled page-item',
                'pageCssClass' => 'page-item',
                'linkOptions' => ['class' => 'page-link'],
                'disabledListItemSubTagOptions' => ['tag' => 'a', 'class' => 'page-link disabled'],
            ],
        ],
    ]); ?>


</div>
