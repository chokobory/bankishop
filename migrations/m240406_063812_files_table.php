<?php

use yii\db\Migration;

/**
 * Class m240406_063812_files_table
 */
class m240406_063812_files_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('files', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->unique(),
            'path' => $this->string(),
            'created_at' => $this->timestamp()->defaultExpression('NOW()'),
            'updated_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')->append('ON UPDATE NOW()'),
        ]);

        $this->createIndex(
            'idx_files_id',
            'files',
            'id',
        );
    }

    public function safeDown()
    {
        $this->dropIndex(
            'idx_files_id',
            'files');

        $this->dropTable('files');
    }

}
