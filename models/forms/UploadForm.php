<?php

namespace app\models\forms;

use app\models\Files;
use Yii;
use yii\base\Model;

class UploadForm extends Model
{
    public $files;

    public function rules()
    {
        return [
            [['files'], 'file', 'skipOnEmpty' => false, 'maxFiles' => 5, 'extensions' => 'jpeg, png, ico, jpg', 'checkExtensionByMimeType' => false],
        ];
    }

    public function attributeLabels()
    {
        return [
            'files' => 'Файлы',
        ];
    }

    public function saveImage()
    {
        foreach ($this->files as $file) {
            $model = new Files();
            $name = $this->checkName(basename($file->name));

            $path = Yii::getAlias('@app') . '/web/images/';
            if (!is_dir($path))
                mkdir($path, 0777, true);

            $savePath = $path . $name;
            $file->saveAs($savePath);

            $model->path = "/images/$name";
            $model->name = $name;
            $model->save();
            var_dump($model->errors);
        }

    }

    private function checkName($fileName)
    {
        $fileName = $this->transliterate($fileName);
        $isExist = Files::findOne(['name' => $fileName]);

        if ($isExist)
            $fileName = microtime() . "_" . $fileName;

        return $fileName;
    }

    private function transliterate($string)
    {
        $string = mb_strtolower($string);

        $transliterationTable = [
            'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e',
            'ё' => 'yo', 'ж' => 'zh', 'з' => 'z', 'и' => 'i', 'й' => 'y', 'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o',
            'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'kh', 'ц' => 'ts', 'ч' => 'ch', 'ш' => 'sh',
            'щ' => 'sch', 'ъ' => '', 'ы' => 'y', 'ь' => '', 'э' => 'e', 'ю' => 'yu', 'я' => 'ya'
        ];
        return strtr($string, $transliterationTable);
    }



}