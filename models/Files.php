<?php

namespace app\models;

/**
 * This is the model class for table "files".
 *
 * @property int $id
 * @property string $name
 * @property string|null $path
 * @property string $created_at
 * @property string $updated_at
 */
class Files extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'files';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'path'], 'string', 'max' => 255],
            [['name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'path' => 'Изображение',
            'created_at' => 'Дата и время загрузки',
            'updated_at' => 'Updated At',
        ];
    }
}
